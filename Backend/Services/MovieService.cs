using System;
using System.Collections.Generic;
using System.Linq;
using Backend.Models;

public class MovieService
{
    private List<Movie> _movies;
    private List<Genre> _genres;

    public MovieService()
    {
        _genres = new List<Genre>();
        var g1 = new Genre { Id = 1, Name = "Action" };
        var g2 = new Genre { Id = 2, Name = "Fantasy" };

        _genres.Add(g1);
        _genres.Add(g2);

        _movies = new List<Movie>();
        _movies.Add(new Movie { Id = 1, Name = "Independence day", Genre = g1 });
        _movies.Add(new Movie { Id = 2, Name = "Avengers Infinity war", Genre = g1 });
        _movies.Add(new Movie { Id = 3, Name = "How to train your dragon", Genre = g2 });
        _movies.Add(new Movie { Id = 4, Name = "How to train your dragon", Genre = g2 });
        _movies.Add(new Movie { Id = 5, Name = "Independence day", Genre = g1 });
        _movies.Add(new Movie { Id = 6, Name = "Avengers Infinity war", Genre = g1 });
        _movies.Add(new Movie { Id = 7, Name = "How to train your dragon", Genre = g2 });
        _movies.Add(new Movie { Id = 8, Name = "How to train your dragon", Genre = g2 });
        _movies.Add(new Movie { Id = 9, Name = "Independence day", Genre = g1 });
        _movies.Add(new Movie { Id = 10, Name = "Avengers Infinity war", Genre = g1 });
        _movies.Add(new Movie { Id = 11, Name = "How to train your dragon", Genre = g2 });
        _movies.Add(new Movie { Id = 12, Name = "How to train your dragon", Genre = g2 });
    }

    internal List<Genre> GetGenres() => _genres;

    // Method that retrieves the movies based on the provided search criteria with paging
    public List<Movie> Get(string name, int genreId, int pageSize, int pageNumber)
    {
        // if name is empty we will just get everything
        var retVal = _movies.Where(m => m.Name.IndexOf(name ?? "", StringComparison.OrdinalIgnoreCase) >= 0);

        // If genre is different from 0 then apply the filter
        // otherwise don't filter based on genre
        if (genreId != 0)
        {
            retVal = retVal?.Where(m => m.Genre.Id == genreId);
        }

        // We are assuming if the user is providing anything else than a zero 
        // he wants paging otherwise, return everything --> don't try in prod
        if (pageSize != 0)
        {
            retVal = retVal.Skip((pageNumber - 1) * pageSize).Take(pageSize);
        }

        return retVal.ToList();
    }

    public Movie Get(int id) => _movies.FirstOrDefault(x => x.Id == id);

    public Movie Create(Movie newMovie)
    {
        newMovie.Id = _movies.Max(x => x.Id) + 1;
        _movies.Add(newMovie);
        return newMovie;
    }

    public Movie Update(Movie movie)
    {
        var m = _movies.FirstOrDefault(x => x.Id == movie.Id);

        m.Name = movie.Name;
        return m;
    }

    public void Delete(int id)
    {
        var m = _movies.FirstOrDefault(x => x.Id == id);
        if (m != null)
            _movies.Remove(m);
    }
}