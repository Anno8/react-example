﻿using System.Collections.Generic;
using Backend.Models;
using Microsoft.AspNetCore.Mvc;

namespace Backend.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class MoviesController : ControllerBase
    {
        private readonly MovieService _movieService;

        public MoviesController(MovieService movieService) =>
            _movieService = movieService;

        [HttpGet]
        public ActionResult<List<Movie>> Get(string name = "", int genreId = 0, int pageSize = 0, int pageNumber = 0) =>
            _movieService.Get(name, genreId, pageSize, pageNumber);


        [HttpGet("{id}")]
        public ActionResult<Movie> Get(int id) =>
            _movieService.Get(id);

        [HttpPost]
        public ActionResult<Movie> Post([FromBody] Movie newMovie) =>
             _movieService.Create(newMovie);

        [HttpPut("{id}")]
        public ActionResult<Movie> Put(int id, [FromBody] Movie movie)
        {
            var m = _movieService.Get(id);
            if (m == null)
            {
                return NotFound($"Movie with the id of: {id} doesn't exist");
            }

            m.Name = movie.Name;
            return m;
        }

        [HttpDelete("{id}")]
        public void Delete(int id)
        {
            var m = _movieService.Get(id);
            if (m == null)
            {
                NotFound($"Movie with the id of: {id} doesn't exist");
            }

            _movieService.Delete(id);
        }
    }
}
