﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Backend.Models;
using Microsoft.AspNetCore.Mvc;

namespace Backend.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class GenresController : ControllerBase
    {
        private readonly MovieService _movieService;

        public GenresController(MovieService moviesService) =>
            _movieService = moviesService;

        [HttpGet]
        public ActionResult<List<Genre>> GetGenres() =>
          _movieService.GetGenres();
    }
}
