import React, { Component } from "react";
import MovieNameFilter from "./Filter/MovieNameFilter";
import MovieGenreFilter from "./Filter/MovieGenreFilter";
import { connect } from "react-redux";
import Paging from "./Paging/Paging";
import List from "./List/List";
import { fetchMovies } from "../../actions/movieActions";
import { fetchGenres } from "../../actions/genreActions";

class Movies extends Component {
  constructor(props) {
    super(props);

    // Initial fetching with the default values
    this.props.fetchGenres();
    this.props.fetchMovies(props.filter.movieName, props.filter.genreId, props.paging.pageSize, props.paging.page);
  }

  componentWillReceiveProps(props) {
    // checking if the new props differ from the old ones, if they do make a get request
    if(props.filter.genreId !== this.props.filter.genreId ||
      props.filter.movieName !== this.props.filter.movieName ||
      props.paging.pageSize !== this.props.paging.pageSize ||
      props.paging.page !== this.props.paging.page){
        this.props.fetchMovies(props.filter.movieName, props.filter.genreId, props.paging.pageSize, props.paging.page);
    }
  }

  render() {
    return (
      <div className="movies">
        <MovieNameFilter />
        <MovieGenreFilter />
        <Paging />
        <List />
      </div>
    );
  }
}

const mapStateToProps = state => ({
  filter: state.filter,
  paging: state.paging
});

const mapDispatchToProps = dispatch => ({
  fetchMovies: (name, genreId, pageSize, pageNumber) => dispatch(fetchMovies(name, genreId, pageSize, pageNumber)),
  fetchGenres: () => dispatch(fetchGenres())
});


export default connect(mapStateToProps, mapDispatchToProps)(Movies);
