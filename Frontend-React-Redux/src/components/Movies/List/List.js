import React from "react";
import { connect } from "react-redux";

const List = (props) => {
    return (
      <div className="list">
        {props.movies.data ? props.movies.data.map(m => 
        <div key={m.id}>
          Name: {m.name}
          <br/>
          Genre: {m.genre.name}
          <hr/>
          </div>) : null}
      </div>
    );
}


const mapStateToProps = state => ({
  movies: state.movies
});

const mapDispatchToProps = dispatch => ({
  
});


export default connect(mapStateToProps, mapDispatchToProps)(List);
