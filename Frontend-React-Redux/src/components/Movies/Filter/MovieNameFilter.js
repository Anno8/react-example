import React from "react";
import { movieNameChanged } from "../../../actions/movieActions";
import { connect } from "react-redux";

const MovieNameFilter  = (props) =>{
    return (
      <input type="text" value={props.filter.movieName} onChange={e => props.movieNameChanged(e.target.value)}/>
    );
  }

const mapStateToProps = state => ({
  genres: state.genres,
  filter: state.filter
});

const mapDispatchToProps = dispatch => ({
  movieNameChanged: name => dispatch(movieNameChanged(name))
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(MovieNameFilter);
