import React from "react";
import { genreChanged } from "../../../actions/movieActions";
import { connect } from "react-redux";

const MovieGenreFilter = props => (
  <select
    value={props.filter.genreId}
    onChange={e => props.genreChanged(e.currentTarget.value)}>
    <option value="0">All Genres</option>
    {props.genres.data
      ? props.genres.data.map(item => (
          <option key={item.id} value={item.id}>
            {item.name}
          </option>
        ))
      : null}
  </select>
);

const mapStateToProps = state => ({
  genres: state.genres,
  filter: state.filter
});

const mapDispatchToProps = dispatch => ({
  genreChanged: genreId => dispatch(genreChanged(genreId))
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(MovieGenreFilter);
