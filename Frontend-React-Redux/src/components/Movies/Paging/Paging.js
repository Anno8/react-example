import React from "react";
import "../../../css/paging.css";
import { connect } from "react-redux";
import { pageChanged, pageSizeChanged } from "../../../actions/movieActions";

const Paging = props => {
  return (
    <div className="pagingContainer">
      Items Per page:
      <select
        value={props.paging.pageSize}
        onChange={e => props.pageSizeChanged(e.currentTarget.value)}>
        <option value={0}>All</option>
        <option value={5}>5 </option>
        <option value={10}>10 </option>
        <option value={15}>15 </option>
        )) : null}
      </select>
      <input type="button" value="<<" onClick={() => props.pageChanged(props.paging.page - 1)}/> {props.paging.page}
      <input type="button" value=">>" onClick={() => props.pageChanged(props.paging.page + 1)} />
    </div>
  );
};

const mapStateToProps = state => ({
  paging: state.paging
});

const mapDispatchToProps = dispatch => ({
  pageChanged: page => dispatch(pageChanged(page)),
  pageSizeChanged: size => dispatch(pageSizeChanged(size))
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Paging);
