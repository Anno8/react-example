import { actions } from "../actions/actions";

const initial = { movieName: "", genreId: 0 };

export const filterReducer = (state = initial, action) => {
  switch (action.type) {
    case actions.MOVIE_NAME_CHANGED:
      return { ...state, movieName: action.data };
    case actions.GENRE_ID_CHANGED:
      return { ...state, genreId: action.data };
    default:
      return { ...state };
  }
};
