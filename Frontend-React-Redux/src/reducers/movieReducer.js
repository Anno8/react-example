import { actions } from "../actions/actions";

// Initial state an empty array
export const movieReducer = (state = [], action) => {
  switch (action.type) {
    case actions.FETCH_MOVIES_COMPLETED:
      return { ...state, data: action.data };
    default:
      return { ...state };
  }
};
