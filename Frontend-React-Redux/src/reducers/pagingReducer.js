import { actions } from "../actions/actions";

const initial = { pageSize: 5, page: 1 };

export const pagingReducer = (state = initial, action) => {
  switch (action.type) {
    case actions.PAGE_SIZE_CHANGED:
      return { ...state, pageSize: action.data };
    case actions.PAGE_CHANGED:
      return { ...state, page: action.data };
    default:
      return { ...state };
  }
};
