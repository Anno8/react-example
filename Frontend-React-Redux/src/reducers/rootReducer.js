import { combineReducers } from "redux";
import { genreReducer } from "./genreReducer";
import { movieReducer } from "./movieReducer";
import { filterReducer } from "./filterReducer";
import { pagingReducer } from "./pagingReducer";


const reducers = combineReducers({
    genres: genreReducer,
    movies: movieReducer,
    filter: filterReducer,
    paging: pagingReducer
})

export default reducers;