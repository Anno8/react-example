import { actions } from "../actions/actions";

export const genreReducer = (state = {}, action) => {
  switch (action.type) {
    case actions.FETCH_GENRES_COMPLETED:
      return { ...state, data: action.data };
    default:
      return { ...state };
  }
};
