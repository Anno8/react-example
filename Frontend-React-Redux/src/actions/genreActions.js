import { apiUrl } from "../constants";
import { actions } from "./actions";

export const fetchGenres = () => dispatch => {
    let url = `${apiUrl}/genres`;

    fetch(url)
        .then(response => response.json())
        .then(data => dispatch({ type: actions.FETCH_GENRES_COMPLETED, data}))
        .catch(err => console.log(err));
}