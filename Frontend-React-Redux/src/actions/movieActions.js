import { apiUrl } from "../constants";
import { actions } from "./actions";

export const fetchMovies = (name, genreId, pageSize, pageNumber) => dispatch => {
    let url = `${apiUrl}/movies?name=${name}&genreId=${genreId}&pageSize=${pageSize}&pageNumber=${pageNumber}`;

    fetch(url)
        .then(response => response.json())
        .then(data => dispatch({ type: actions.FETCH_MOVIES_COMPLETED, data}))
        .catch(err => console.log(err));
}

// Some of those methods should be extracted into their own files, but for convenience sake I am leaving them here
export const genreChanged = (genreId) => dispatch => 
    dispatch({ type: actions.GENRE_ID_CHANGED, data: genreId});

export const pageChanged = (page) => dispatch =>
    dispatch({ type: actions.PAGE_CHANGED, data: page});

export const pageSizeChanged = (pageSize) => dispatch =>
    dispatch({ type: actions.PAGE_SIZE_CHANGED, data : pageSize});

export const movieNameChanged = (movieName) => dispatch => 
    dispatch({ type: actions.MOVIE_NAME_CHANGED, data : movieName});