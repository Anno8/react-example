import React, { Component } from "react";
import "../css/movie.css";

class Movie extends Component {
  constructor(props) {
    super(props);
   
    // fetch movie by id just so that we always get the up to date movie
    // in case someone changed it
  }

  render() {
    return (
      <div className="movie">
        <p>Id: {this.props.movie.id}</p>
        <p>Name: {this.props.movie.name}</p>
        <input onClick={_ => this.props.delete(this.props.movie.id)} type="button" value="Delete"/>
        <input onClick={_ => this.props.edit(this.props.movie)} type="button" value="Edit"/>
      </div>
    );
  }
}

export default Movie;
