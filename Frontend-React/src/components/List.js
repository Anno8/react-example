import React, { Component } from "react";
import "../css/App.css";
import Movie from "./Movie";
import { apiUrl } from "../constants";
import EditMovie from "./EditMovie";

class List extends Component {
  constructor(props) {
    super(props);
    this.state = {
      moviesList: []
    };
    
    this.getAllMovies();
    this.delete = this.delete.bind(this);
    this.edit = this.edit.bind(this);
    this.save = this.save.bind(this);
    this.addNewMovie = this.addNewMovie.bind(this);
  }

  getAllMovies() {
    fetch(`${apiUrl}/movies`)
      .then(result => result.json())
      .then(data => this.setState({ moviesList: data }))
      .catch(error => console.log(error));
  }

  delete(id) {
    fetch(`${apiUrl}/movies/${id}`, { method: "DELETE"})
      .then(_ => this.getAllMovies());
  }

  edit(movie){
    this.setState({ movie: movie});
  }

  save(movie){
    let headers = new Headers();
    headers.append("Content-Type", "application/json");

    if(movie.id === 0){
      fetch(`${apiUrl}/movies`,{
        method: "POST",
        body: JSON.stringify(movie),
        headers: headers
      })
        .then(_ => this.getAllMovies())
        .catch(err => console.log(err));

    }
    else {
      fetch(`${apiUrl}/movies/${movie.id}`,{
        method: "PUT",
        body: JSON.stringify(movie),
        headers: headers
      })
        .then(result => result.json()
        .then(_ => this.getAllMovies()))
        .catch(err => console.log(err));
    }

    this.setState({movie: undefined});
  }

  addNewMovie(){
      this.setState({
          movie: {id: 0, name: ""}
      });
  }

  render() {
    return (
      <div className="list">
        <input type="button" value="Add new movie" onClick={this.addNewMovie}/>
        <EditMovie movie={this.state.movie} save={this.save}/>
        {this.state.moviesList.map(m => 
          <Movie key={m.id} movie={m} delete={this.delete} edit={this.edit}/>
        )}
      </div>
    );
  }
}

export default List;
