import React, { Component } from "react";
import "../css/movie.css";

class EditMovie extends Component {
  constructor(props) {
    super(props);
  }

  componentWillReceiveProps(props){
    this.setState({
        movie: props.movie ? props.movie: {id: 0, name: ""}
    });
  }
  handleMovieNameChange(event){
    let m = this.state.movie;
    m.name = event.target.value;
    this.setState({
      movie: m
    });
  }

  render() {
    return (
    this.props.movie ? 
      <div >
        <p>Name: <input type="text" value={this.state.movie.name} onChange={event => this.handleMovieNameChange(event)}/></p> 
        <input onClick={_ => this.props.save(this.state.movie)} type="button" value="Save"/>
      </div> : null
    );
  }
}

export default EditMovie;
