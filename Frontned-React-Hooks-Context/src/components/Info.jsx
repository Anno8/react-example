import React, { useContext } from "react";
import { ApplicationContext } from "../contexts/ApplicationContext";
import "./Info.css";

export const Info = () => {
  const appData = useContext(ApplicationContext);
  return (
    <div>
      <h2>Dummy component</h2>
      <div className="box" />
      <br />
      Theme: {appData.theme}
      <br />
      <button onClick={() => appData.setTheme("light")}>Change to light</button>
      <button onClick={() => appData.setTheme("dark")}>Change to dark</button>
    </div>
  );
};
