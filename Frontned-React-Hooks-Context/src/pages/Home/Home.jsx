import React, { useContext, useState } from "react";
import { UserContext } from "../../contexts/UserContext";


export const Home = () => {
  const {user, login, logout} = useContext(UserContext);
  const [name, setName] = useState("")
  return (
    <div>
      <h1>Current user: {user || "Not logged in"}</h1>
      <input type="text" value={name} onChange={(e) => setName(e.target.value)}/>
      <input type="button" onClick={() => login(name)} value="Sign in"/>
      <br/>
      <br/>
      <br/>
      <input type="button" onClick={logout} value="Sign out"/>
    </div>
  );
};
