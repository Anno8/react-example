import { ApplicationContext } from "../../contexts/ApplicationContext";
import { UserContext } from "../../contexts/UserContext";
import React, { useContext } from "react";

export const Settings = () => {
  const { user } = useContext(UserContext);
  const { theme, setTheme } = useContext(ApplicationContext);
  const radioHandler = (value) => ({
    checked: theme === value,
    value: value,
    onChange: ({ target }) => setTheme(target.value)
  })
  return (
    <div>
      <h1>Settings</h1>
      <p>Select a theme</p>

      <input type="radio" name="theme" {...radioHandler("dark")} /> Dark <br />
      <input type="radio" name="theme" {...radioHandler("light")} /> Light
      <h1>Current user: {user || "Not logged in"}</h1>
    </div>
  );
};
