import React from "react";
import { Info } from "../../components/Info";

export const About = () => (
  <div>
    <h1>About</h1>
    <Info/>
  </div>
);
