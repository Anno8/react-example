import React, { useState } from "react";
import { AppNavigation } from "./navigation/AppNavigation";
import { ApplicationProvider } from "./contexts/ApplicationContext";
import { UserProvider } from "./contexts/UserContext";
import "./App.css";

export const App = () => {
  const [theme, setTheme] = useState("light");
  const [user, setUser] = useState(null);

  return (
    <div className={`app ${theme}`}>
      <ApplicationProvider
        value={{
          theme,
          setTheme: theme => setTheme(theme)
        }}
      >
        <UserProvider
          value={{
            user,
            login: user => setUser(user),
            logout: () => setUser(null)
          }}
        >
          <AppNavigation />
        </UserProvider>
      </ApplicationProvider>
    </div>
  );
};
